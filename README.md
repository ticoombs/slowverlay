## Slowverlay - Slowb.ro Gentoo Overlay

Mainly used for me to get the latest versions of software that don't exist in the portage gentoo repository. 
I attempt to upstream my changes if needed.

Actively Updated:

- gui-apps/i3-focus-last
  - Can be used on Sway!
- x11-misc/ydotool
  - Automate mouse & keyboard events


Outdated:

- x11-misc/synergy
  - Wayland support does not exist, so dropping until they do
- mail-client/thunderbird
  - moved to thunderbird-bin

