# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cargo

PROPERTIES+=" live"

DESCRIPTION="Swap between different windows. Alt-Tab but for linux"
HOMEPAGE="https://github.com/lbonn/i3-focus-last"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

inherit git-r3
EGIT_REPO_URI="https://github.com/lbonn/i3-focus-last"
EGIT_BRANCH="master"

src_unpack() {
	if [[ "${PV}" == *9999* ]]; then
		git-r3_src_unpack
		cargo_live_src_unpack
	else
		cargo_src_unpack
	fi
}

src_configure() {
	cargo_src_configure
}

src_compile() {
	cargo_src_compile
}

src_install() {
	cargo_src_install

}
src_test() {
	cargo_src_test
}

pkg_postinst() {
	einfo "To get the most out of ${PN}, add the following to your i3/sway config:"
	einfo "  exec_always i3-focus-last server"
	einfo "  bindsym $mod+Tab exec i3-focus-last"
	einfo ""
	einfo "There are several packages that you may find useful with ${PN}"
	einfo "  gui-apps/waybar"
	einfo "  x11-misc/rofi"
	einfo "Please refer to their description for additional info."
}
