# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit eutils flag-o-matic gnome2-utils cmake git-r3

DESCRIPTION="Lets you easily share a single mouse and keyboard between multiple computers"
HOMEPAGE="https://symless.com/synergy https://github.com/symless/synergy-core"
EGIT_REPO_URI="https://github.com/symless/${PN}-core/"
EGIT_COMMIT_URI="37bbd3c0ee7708c0bf26bebcc115d02c97bbbe38"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~ppc ppc64 ~sparc x86 ~amd64-linux ~x86-linux ~sparc-solaris ~x86-solaris"
IUSE=""

#S=${WORKDIR}/${PN}-core-${PV}-stable

COMMON_DEPEND="
	net-misc/curl
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXi
	x11-libs/libXinerama
	x11-libs/libXrandr
	x11-libs/libXtst
	dev-libs/openssl
"
DEPEND="
	${COMMON_DEPEND}
	x11-base/xorg-proto
"
RDEPEND="
	${COMMON_DEPEND}
"

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	append-cxxflags ${mycmakeargs}
	local mycmakeargs=(  -DSYNERGY_REVISION=0bd448d5 )
	cmake_src_configure
}

src_compile() {
	cmake_src_compile
}

src_install() {
	dobin ../${P}_build/bin/${PN}{c,s} ../${P}_build/bin/synergy

	insinto /etc
	newins doc/synergy.conf.example synergy.conf

	newman doc/${PN}c.man ${PN}c.1
	newman doc/${PN}s.man ${PN}s.1

	dodoc doc/synergy.conf.example* ChangeLog
}
